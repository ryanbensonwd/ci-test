var app = require('../app');

var assert = require('assert');

describe('App Calculator', function() {
  describe('Multiply', function() {
    it('Should return a * b', function() {
      assert.equal(16, app(2, 8));
    });

    it('Should return a * b', function() {
      assert.equal(32, app(4, 8));
    });
  });
});

describe('Array', function() {
  describe('#indexOf()', function () {
    it('should return -1 when the value is not present', function () {
      assert.equal(-1, [1,2,3].indexOf(5));
      assert.equal(-1, [1,2,3].indexOf(0));
    });
  });
});
