#!/usr/bin/env node

var mult = function(a, b) {
  return a * b;
};

module.exports = mult;
